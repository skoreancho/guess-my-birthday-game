from random import randint

#create prompt that asks for name
name = input("Hi! What is your name? ")

high = 2004
low = 1924
random_month = randint(1, 12)
random_year = randint(low, high)

#create a for loop with a range of 5
for guess in range(5):
    #create prompt that guesses birthday
    print(f"Guess: {guess + 1}, {name} were you born in {random_month}/{random_year}?")
    response = input("yes or no? ")
    #create conditional for correct case
    if (response == "yes"):
        print("I knew it!")
        break
    #create conditional for last incorrect case
    elif (guess == 4):
        print("I have other things to do. Good bye.")
    #create conditional for every other incorrect case
    else:
        print("Drat! Lemme try again!")
        detail = input("Are you younger or older? ")
        if (detail == "younger"):
            low = random_year + 1
            random_year = randint(low, high)
            random_month = randint(1, 12)
        else:
            high = random_year - 1
            random_year = randint(low, high)
            random_month = randint(1, 12)
